from django.apps import AppConfig


class ProfessionalXpConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'professional_xp'
