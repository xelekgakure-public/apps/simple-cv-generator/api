from django.db import models


class Quality(models.Model):
    class Meta:
        verbose_name = 'Quality'
        verbose_name_plural = 'Qualities'
        
    name = models.CharField(max_length=255, unique=True)
    
    def __str__(self) -> str:
        return self.name


class Defect(models.Model):
    name = models.CharField(max_length=255, unique=True)
    
    def __str__(self) -> str:
        return self.name