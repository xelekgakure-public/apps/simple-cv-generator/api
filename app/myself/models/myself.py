from django.db import models
from django.contrib.auth.models import User

from core.models.address import Address
from core.models.gender import Gender


class MySelf(models.Model):
    class Meta:
        verbose_name = 'Myself'
        verbose_name_plural = 'Peoples'
        
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, editable=False)
    gender = models.ForeignKey(Gender, on_delete=models.SET_NULL, null=True)
    
    firstname = models.CharField(max_length=255, null=False, blank=False)
    lastname = models.CharField(max_length=255, null=False, blank=False)
    
    birthdate = models.DateField(null=False)
    
    phone = models.CharField(max_length=12, null=True, blank=True)
    email = models.EmailField(max_length=255, null=False)
    address = models.ForeignKey(Address, on_delete=models.SET_NULL, null=True)
    social_medias = models.ManyToManyField("SocialMedia")
    
    first_job_date = models.DateField()
    title = models.CharField(max_length=255)
    
    positive_points = models.ManyToManyField("Quality")
    negative_points = models.ManyToManyField("Defect")
