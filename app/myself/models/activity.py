from django.db import models


class Activity(models.Model):
    class Meta:
        verbose_name = 'Activity'
        verbose_name_plural = 'Activities'
        
    name = models.CharField(max_length=255, unique=True)
    
    def __str__(self) -> str:
        return self.name