from django.db import models
from django.contrib.staticfiles import finders

class SocialMedia(models.Model):
    name = models.CharField(max_length=255, unique=True)
    uri = models.TextField()
        
    def get_uri(self, identifier: str):
        return f'{self.uri}{"" if self.uri.endswith("/") else "/"}{identifier}'
    
    @property
    def icon(self):
        icons = finders.find(f'icons/social-medias/{self.name}.svg')
        if len(icons) > 0:
            return icons[0]
        return finders.find(f'icons/social-medias/default.svg')[0]
    
    