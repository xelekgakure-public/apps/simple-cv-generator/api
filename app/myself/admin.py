from django.contrib import admin

from .models.activity import Activity
from .models.myself import MySelf
from .models.quality import Quality, Defect
from .models.social_media import SocialMedia

# Register your models here.


@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):
    pass

@admin.register(Quality)
class QualityAdmin(admin.ModelAdmin):
    pass


@admin.register(Defect)
class DefectAdmin(admin.ModelAdmin):
    pass


@admin.register(SocialMedia)
class SocialMediaAdmin(admin.ModelAdmin):
    pass


@admin.register(MySelf)
class MySelfAdmin(admin.ModelAdmin):
    empty_value_display = "-empty-"
    
    readonly_fields = ["user"]
    list_display = ("id", "firstname", "lastname", "_gender", "phone", "email")
    
    fieldsets = [
        (
            "My Identity",
            {
                "fields": [("firstname", "lastname"), "gender", "birthdate"],
            },
        ),
        (
            "Contact",
            {
                "fields": [("phone", "email")],
            },
        ),
        (
            "Professional Information",
            {
                "fields": ["first_job_date", "title"],
            },
        ),
        (
            "Other Informations",
            {
                "classes": ["collapse"],
                "fields": ["positive_points", "negative_points", "social_medias"],
            },
        ),
    ]
    
    @admin.display(ordering='gender__id', description='Gender')
    def _gender(self, obj: MySelf):
        return obj.gender.name