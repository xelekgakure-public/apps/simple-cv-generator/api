from datetime import date
from django.shortcuts import render

from rest_framework.response import Response
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer

CV_DATA = {
    "title": "Expert en informatique et Système d'information",
    "profile_image": "images/person/julien-lauret.jpg",
    "description": "Développeur Full Stack en recherche de nouvel emploi en CDI",
    "identity": {
        "firstname": "julien",
        "lastname": "lauret" ,
        "birthdate": date(1998, 11, 24),
    },
    "address": {
        "address_1": "21 rue Simone Boudet",
        "address_2": "appt. C80",
        "postal_code": 31200,
        "city": "Toulouse",
        "country": "France"
    },
    "contact": {
        "phone": "06.85.57.17.26",
        "email": "julien.lauret.pro@gmail.com"
    },
    "skills": {
        "languages": [
            {
                "name": "français",
                "level": 100
            },
            {
                "name": "anglais",
                "level": 80
            },
            {
                "name": "espagnol",
                "level": 30
            },
            {
                "name": "japonais",
                "level": 10
            },
        ],
        "techno": [
            {
                "name": "Python - Django/FastApi/Flask",
                "mastery": "90%",
                "icon": "fa-brands fa-python"
            },
            {
                "name": "PHP - Symfony/CakePHP",
                "mastery": "78%",
                "icon": "fa-brands fa-php"
            },
            {
                "name": "Base de Donnée - MySQL/MariaDB",
                "mastery": "65%",
                "icon": "fa-solid fa-database"
            },
            {
                "name": "Développement Web (Front)",
                "mastery": "80%",
                "icon": "fa-solid fa-globe"
            },
            {
                "name": "Conteneurisation - Docker/Podman",
                "mastery": "75%",
                "icon": "fa-brands fa-docker"
            },
            {
                "name": "CI/CD - GitlabCI",
                "mastery": "60%",
                "icon": "fa-brands fa-gitlab"
            }
        ]
    },
    "formations": [
        {
            "school": "EPSI Bordeaux",
            "dates": {
                "start": date(2020, 10, 11),
                "end": date(2021, 7, 31)
            },
            "licence": "BAC +5 - Titre de niveau 7 - Expert en informatique et Systèmes d'informations"
        },
        {
            "school": "EPSI Bordeaux",
            "dates": {
                "start": date(2018, 10, 9),
                "end": date(2019, 7, 4)
            },
            "licence": "BAC +3 - Titre de niveau 5 - Concepteur dévelopeur informatique"
        },
        {
            "school": "YNOV - Ingésup Bordeaux",
            "dates": {
                "start": date(2016, 9, 21),
                "end": date(2018, 6, 30)
            },
            "licence": "BAC +1/+2 Informatique"
        },
    ],
    "professional_xp": [
        {
            "type": "CDI",
            "company": "ASTEK",
            "city": "Toulouse",
            "postal_code": "31",
            "dates": {
                "start": date(2023, 7, 3),
                "end": date(2024, 5, 28)
            },
            "tasks": [
                "Développement et maintenance d'application python sur site client",
                "Gestion de CI/CD Gitlab"
            ]
        },
        {
            "type": "CDI",
            "company": "Vega France",
            "city": "Toulouse",
            "postal_code": "31",
            "dates": {
                "start": date(2021, 10, 11),
                "end": date(2023, 6, 27)
            },
            "tasks": [
                "Gestion, Développement et maintenance d'application Web",
                "Développement de logiciel embarqués",
                "Administration Système et Réseau",
                "Electronique",
                "Gestion de CI/CD Gitlab"
            ]
        },
        {
            "type": "Alternance",
            "company": "Job Explorer",
            "city": "Bordeaux",
            "postal_code": "33",
            "dates": {
                "start": date(2019, 1, 19),
                "end": date(2021, 8, 31)
            },
            "tasks": [
                "Développement et maintenance application web",
                "Migration CakePHP vers Symfony",
                "Support client"
            ]
        },
        {
            "type": "Stage",
            "company": "CHD - La Candelie",
            "city": "Agen",
            "postal_code": "47",
            "dates": {
                "start": date(2018, 6, 1),
                "end": date(2018, 8, 31)
            },
            "tasks": [
                "Développement Web - Flask",
                "Initiation à l'architechture d'infra réseau"
            ]
        },
        {
            "type": "Stage",
            "company": "France3 Poitou-Charentes",
            "city": "Poitiers",
            "postal_code": "86",
            "dates": {
                "start": date(2017, 6, 1),
                "end": date(2017, 8, 31)
            },
            "tasks": [
                "Maintenance Réseau physique",
                "Initiation à l'outil GLPI"
            ]
        },
    ],
    "others": {
        "hobbies": [
            "Jeux vidéos",
            "Jeux de Rôles",
            "Musique",
            "Montage Vidéo",
            "Dessin",
            "Manga/Anime",
            "Développement"
        ],
        "patronage": [
            {
                "name": "Pets Rescue France",
                "url": "https://sos-animaux-23.fr/",
                "sector": "Développement web",
                "dates": {
                    "start": date(2024, 4, 1),
                    "end": date(2024, 5, 28)
                },
                "referants": [
                    {
                        "name": "MARYSE CERQUEIRA",
                        "contact": "mcerqueira@groupeastek.fr"
                    },
                    {
                        "name": "Clément Trofleau",
                        "contact": "c.trofleau@gmail.com"
                    }
                ],
                "tasks": [
                    "Projet Pattes-a-coeur: Review et correction de merge request",
                    "Projet de générateur Formulaire de don: Développement d'api personnalisée sous FastApi"
                ],
            },
        ], 
        "links": [
            {
                "name": "Gitlab",
                "icon": "fa-brands fa-gitlab",
                "id": "@XelekGakure",
                "url": "https://gitlab.com/XelekGakure"
            },  
            {
                "name": "Github",
                "icon": "fa-brands fa-github",
                "id": "@XelekGakure",
                "url": "https://github.com/XelekGakure"
            },  
            {
                "name": "LinkedIn",
                "icon": "fa-brands fa-linkedin",
                "id": "Julien LAURET",
                "url": "https://www.linkedin.com/in/xelekgakure/"
            },  
        ]
    }
}

@api_view(('GET',))
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def preview(request):
    display = request.GET.get("display", None)
    if request.GET.get("format", "").lower() == "json":
        return Response(data=CV_DATA)
    return render(request, "preview.html", context={
        **CV_DATA,
        "raw": display == "raw"
    })