from django.db import models
from myself.models.myself import MySelf


class CuriculumVitae(models.Model):
    subject = models.ForeignKey(MySelf, on_delete=models.SET_NULL, null=True)
    uid = models.UUIDField(auto_created=True, null=False)
    version = models.IntegerField(default=0, null=False)
    
    draft = models.BooleanField(default=False, null=False)
    
    def __str__(self) -> str:
        return f""
