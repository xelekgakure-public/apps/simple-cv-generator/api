from django.urls import path

from .views import previews_views

urlpatterns = [
    path('preview/', previews_views.preview, name="cv-preview")
]