from django.db import models


class Config(models.Model):
    key = models.CharField(max_length=255, null=False, primary_key=True)
    value = models.TextField(null=True)
    
    @classmethod
    def get(cls, key: str, default=None):
        try:
            return cls.objects.get(pk=key)
        except:
            return default