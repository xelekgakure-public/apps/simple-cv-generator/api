from django.db import models


class Gender(models.Model):
    name = models.CharField(max_length=255, unique=True)
    shortname = models.CharField(max_length=3)
    
    def __str__(self) -> str:
        return self.name