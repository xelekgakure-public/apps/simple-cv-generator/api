from django.db import models


class Address(models.Model):
    class Meta:
        verbose_name = 'Address'
        verbose_name_plural = 'Addresses'
        
    city = models.CharField(max_length=255, null=False)
    postal_code = models.PositiveIntegerField()
    
    address_1 = models.CharField(max_length=255, null=False)
    address_2 = models.CharField(max_length=255, null=False, default='')
    
    country_code = models.IntegerField()
    
    
    def __str__(self) -> str:
        return f"{self.address_1}, {self.address_2} - {self.postal_code} {self.city}"