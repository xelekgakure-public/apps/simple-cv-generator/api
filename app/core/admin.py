from django.contrib import admin

# Register your models here.
from .models.address import Address
from .models.config import Config
from .models.gender import Gender


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    pass

@admin.register(Config)
class ConfigAdmin(admin.ModelAdmin):
    pass

@admin.register(Gender)
class GenderAdmin(admin.ModelAdmin):
    pass
